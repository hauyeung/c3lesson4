﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace csharp3_lesson4
{
    class Line: Shape
    {
         public Line(Rectangle rectangle, Color shapeColor, bool filled)
            {
                base.X = rectangle.X;
                base.Y = rectangle.Y;
                base.Width = rectangle.Width;
                base.Height = rectangle.Height;
                base.ShapeColor = shapeColor;
                base.Filled = filled;
            }

            public override void draw(Graphics graphics)
            {
                graphics.DrawLine(new Pen(base.ShapeColor),base.X,base.Y,base.X+base.Width,base.Y+base.Height);
            }
    }
}
