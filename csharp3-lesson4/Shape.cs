﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace csharp3_lesson4
{
    public class Shape
    {
        public Color ShapeColor;
        public bool Filled;
        public int X;
        public int Y;
        public int Width;
        public int Height;

        public virtual void draw(Graphics graphics)
        {
            // Override this method in the child class
        }

    }
}
